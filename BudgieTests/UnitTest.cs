using System;
using System.Collections.Generic;
using Budgie.Extensions;
using Budgie.Models;
using NUnit.Framework;

namespace BudgieTests
{
    public class Tests
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void TestGetCurrentBudgetDay()
        {
            User user = new User
            {
                Budget = 10,
                BudgetPeriod = "DAY",
                HasBudgetRollover = true,
                BudgetRolloverFrom = DateTimeOffset.Parse("2019-11-01T15:08:00+08:00")
            };
            
            IEnumerable<Expense> expenses = new List<Expense>
            {
                new Expense { Amount = 10, Date = DateTimeOffset.Parse("2019-11-02T15:08:00+08:00") },
                new Expense { Amount = 10, Date = DateTimeOffset.Parse("2019-11-05T15:08:00+08:00") },
                new Expense { Amount = 10, Date = DateTimeOffset.Parse("2019-11-06T15:08:00+08:00") },
                new Expense { Amount = 10, Date = DateTimeOffset.Parse("2019-11-08T15:08:00+08:00") },
                new Expense { Amount = 10, Date = DateTimeOffset.Parse("2019-11-11T15:08:00+08:00") }
            };
            DateTimeOffset now = DateTimeOffset.Parse("2019-11-12T15:08:00+08:00");

            double actual = user.GetCurrentBudget(expenses, now);
            double expected = 70;

            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void TestGetCurrentBudgetWeek()
        {
            User user = new User
            {
                Budget = 50,
                BudgetPeriod = "WEEK",
                HasBudgetRollover = true,
                BudgetRolloverFrom = DateTimeOffset.Parse("2019-09-11T15:08:00+08:00")
            };

            IEnumerable<Expense> expenses = new List<Expense>
            {
                new Expense { Amount = 50, Date = DateTimeOffset.Parse("2019-09-18T15:08:00+08:00") },
                new Expense { Amount = 50, Date = DateTimeOffset.Parse("2019-10-18T15:08:00+08:00") },
                new Expense { Amount = 50, Date = DateTimeOffset.Parse("2019-11-08T15:08:00+08:00") }
            };
            DateTimeOffset now = DateTimeOffset.Parse("2019-11-15T15:08:00+08:00");

            double actual = user.GetCurrentBudget(expenses, now);
            double expected = 350;

            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void TestGetCurrentBudgetMonth()
        {
            User user = new User
            {
                Budget = 300,
                BudgetPeriod = "MONTH",
                HasBudgetRollover = true,
                BudgetRolloverFrom = DateTimeOffset.Parse("2019-09-11T15:08:00+08:00")
            };

            IEnumerable<Expense> expenses = new List<Expense>
            {
                new Expense { Amount = 50, Date = DateTimeOffset.Parse("2019-09-18T15:08:00+08:00") },
                new Expense { Amount = 50, Date = DateTimeOffset.Parse("2019-10-18T15:08:00+08:00") },
                new Expense { Amount = 50, Date = DateTimeOffset.Parse("2019-11-08T15:08:00+08:00") }
            };
            DateTimeOffset now = DateTimeOffset.Parse("2019-12-12T15:08:00+08:00");

            double actual = user.GetCurrentBudget(expenses, now);
            double expected = 1050;

            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void TestGetCurrentBudgetYear()
        {
            User user = new User
            {
                Budget = 1000,
                BudgetPeriod = "YEAR",
                HasBudgetRollover = true,
                BudgetRolloverFrom = DateTimeOffset.Parse("2015-09-11T15:08:00+08:00")
            };

            IEnumerable<Expense> expenses = new List<Expense>
            {
                new Expense { Amount = 500, Date = DateTimeOffset.Parse("2015-09-18T15:08:00+08:00") },
                new Expense { Amount = 500, Date = DateTimeOffset.Parse("2017-10-18T15:08:00+08:00") },
                new Expense { Amount = 500, Date = DateTimeOffset.Parse("2019-11-08T15:08:00+08:00") }
            };
            DateTimeOffset now = DateTimeOffset.Parse("2019-12-12T15:08:00+08:00");

            double actual = user.GetCurrentBudget(expenses, now);
            double expected = 4000;

            Assert.AreEqual(expected, actual);
        }
    }
}