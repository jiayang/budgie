using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Amazon.SimpleEmail;
using Budgie.Models.Settings;
using Budgie.Services;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.CookiePolicy;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace Budgie
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<PostgreSQLSettings>(Configuration.GetSection(nameof(PostgreSQLSettings)));
            services.Configure<RedisSettings>(Configuration.GetSection(nameof(RedisSettings)));
            services.Configure<AWSSettings>(Configuration.GetSection(nameof(AWSSettings)));

            services.AddSingleton<IPostgreSQLSettings>(serviceProvider => serviceProvider.GetRequiredService<IOptions<PostgreSQLSettings>>().Value);
            services.AddSingleton<IRedisSettings>(serviceProvider => serviceProvider.GetRequiredService<IOptions<RedisSettings>>().Value);
            services.AddSingleton<IAWSSettings>(serviceProvider => serviceProvider.GetRequiredService<IOptions<AWSSettings>>().Value);

            services.AddSingleton<ExpenseService>();
            services.AddSingleton<UserService>();
            services.AddSingleton<LocationService>();
            services.AddSingleton<AccessCodeService>();
            services.AddSingleton<EmailService>();
            services.AddSingleton<FirebaseService>();

            services.AddStackExchangeRedisCache(options =>
            {
                options.Configuration = Configuration.GetSection(nameof(RedisSettings)).Get<RedisSettings>().Host;
                options.InstanceName = Configuration.GetSection(nameof(RedisSettings)).Get<RedisSettings>().Name;
            });

            services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            })
            .AddJwtBearer(options =>
            {
                options.Authority = $"https://securetoken.google.com/{Configuration.GetSection(nameof(FirebaseSettings)).Get<FirebaseSettings>().ProjectID}";
                options.Audience = Configuration.GetSection(nameof(FirebaseSettings)).Get<FirebaseSettings>().ProjectID;
            });

            services.AddControllers();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseCors(builder =>
            {
                builder.WithOrigins("http://localhost:8080", "http://localhost:5000", "https://budgie-1624.web.app", "https://budgie-1624.firebaseapp.com", "https://budgie.testflight.xyz")
                    .AllowAnyHeader()
                    .AllowAnyMethod()
                    .AllowCredentials();
            });
            //app.UseHttpsRedirection();
            app.UseRouting();
            app.UseAuthentication();
            app.UseAuthorization();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
