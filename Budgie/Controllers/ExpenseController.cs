﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Budgie.Commons;
using Budgie.Extensions;
using Budgie.Models;
using Budgie.Services;
using Budgie.Structs;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Distributed;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace Budgie.Controllers
{
    [Authorize]
    [Route("api/expenses")]
    public class ExpenseController : ControllerBase
    {
        private readonly ExpenseService expenseService;
        private readonly LocationService locationService;
        private readonly IDistributedCache cache;

        public ExpenseController(ExpenseService expenseService, LocationService locationService, IDistributedCache cache)
        {
            this.expenseService = expenseService;
            this.locationService = locationService;
            this.cache = cache;
        }

        [HttpPost("")]
        public IActionResult Create()
        {
            string name = Request.Form["name"];
            string amount = Request.Form["amount"];
            string date = Request.Form["date"];
            string colour = Request.Form["colour"];
            string notes = Request.Form["notes"];
            string locationName = Request.Form["location_name"];
            string locationAddress = Request.Form["location_address"];
            string latitude = Request.Form["latitude"];
            string longitude = Request.Form["longitude"];
            string photoUrl = Request.Form["photo_url"];

            int userId = int.Parse(HttpContext.User.Identity.Name);

            Expense expense = new Expense
            {
                Name = name,
                Amount = double.Parse(amount),
                Date = DateTimeOffset.Parse(date),
                Colour = colour,
                Notes = notes,
                PhotoURL = photoUrl,
                UserID = userId
            };

            int expenseId = expenseService.Create(expense);

            if (!string.IsNullOrWhiteSpace(locationName))
            {
                Location location = new Location
                {
                    Name = locationName,
                    Address = locationAddress,
                    Latitude = double.Parse(latitude),
                    Longitude = double.Parse(longitude),
                    ExpenseID = expenseId
                };

                locationService.Create(location);
            }

            if (expense.Date.Date < DateTimeOffset.UtcNow.Date)
            {
                cache.Remove(CacheKey.CurrentBudget(userId.ToString()));
            }

            return Ok(new { id = expenseId });
        }

        [HttpGet("")]
        public IActionResult ReadAll(string date_from = "", string date_to = "", string[] colours = null)
        {
            int userId = int.Parse(HttpContext.User.Identity.Name);

            IEnumerable<Expense> expenses;

            if (!string.IsNullOrWhiteSpace(date_from))
            {
                colours ??= new string[0];

                QueryFilters queryFilters = new QueryFilters(
                    dateFrom: DateTimeOffset.Parse(date_from),
                    dateTo: DateTimeOffset.Parse(date_to),
                    colours: colours
                );

                expenses = expenseService.ReadAll(userId, queryFilters);
            }
            else
            {
                expenses = expenseService.ReadAll(userId);
            }

            return Ok(new { expenses });
        }

        [HttpGet("{id:int}")]
        public IActionResult Read(int id)
        {
            Expense expense = expenseService.Read(id);

            if (expense != null)
            {
                return Ok(new { expense });
            }

            return NoContent();
        }

        [HttpPut("{id:int}")]
        public IActionResult Update(int id)
        {
            string name = Request.Form["name"];
            string amount = Request.Form["amount"];
            string date = Request.Form["date"];
            string colour = Request.Form["colour"];
            string notes = Request.Form["notes"];
            string locationName = Request.Form["location_name"];
            string locationAddress = Request.Form["location_address"];
            string latitude = Request.Form["latitude"];
            string longitude = Request.Form["longitude"];
            string photoUrl = Request.Form["photo_url"];

            int userId = int.Parse(HttpContext.User.Identity.Name);

            Expense expense = new Expense
            {
                ID = id,
                Name = name,
                Amount = double.Parse(amount),
                Date = DateTimeOffset.Parse(date),
                Colour = colour,
                Notes = notes,
                PhotoURL = photoUrl
            };

            expenseService.Update(expense);

            if (!string.IsNullOrWhiteSpace(locationName))
            {
                Location location = new Location
                {
                    Name = locationName,
                    Address = locationAddress,
                    Latitude = double.Parse(latitude),
                    Longitude = double.Parse(longitude),
                    ExpenseID = id
                };

                locationService.Upsert(location);
            }
            else
            {
                locationService.DeleteByExpenseID(id);
            }

            if (expense.Date.Date < DateTimeOffset.UtcNow.Date)
            {
                cache.Remove(CacheKey.CurrentBudget(userId.ToString()));
            }

            return Ok(new { id });
        }

        [HttpDelete("{id:int}")]
        public IActionResult Delete(int id)
        {
            int userId = int.Parse(HttpContext.User.Identity.Name);

            locationService.DeleteByExpenseID(id);
            expenseService.Delete(id);

            cache.Remove(CacheKey.CurrentBudget(userId.ToString()));

            return Ok();
        }
    }
}
