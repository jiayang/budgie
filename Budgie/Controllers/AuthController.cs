﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Security.Claims;
using System.Threading.Tasks;
using Amazon.SimpleEmail.Model;
using Budgie.Commons;
using Budgie.Extensions;
using Budgie.Models;
using Budgie.Services;
using FirebaseAdmin;
using FirebaseAdmin.Auth;
using Google.Apis.Auth.OAuth2;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Distributed;
using Newtonsoft.Json;

namespace Budgie.Controllers
{
    [Authorize]
    [Route("api/auth")]
    public class AuthController : ControllerBase
    {
        private readonly UserService userService;
        private readonly AccessCodeService accessCodeService;
        private readonly EmailService emailService;
        private readonly FirebaseService firebaseService;
        private readonly IDistributedCache cache;
        private readonly IWebHostEnvironment environment;

        public AuthController(UserService userService, AccessCodeService accessTokenService, EmailService emailService, FirebaseService firebaseService, IDistributedCache cache, IWebHostEnvironment environment)
        {
            this.userService = userService;
            this.accessCodeService = accessTokenService;
            this.emailService = emailService;
            this.firebaseService = firebaseService;
            this.cache = cache;
            this.environment = environment;
        }

        [AllowAnonymous]
        [HttpPost("verify_email")]
        public async Task<IActionResult> VerifyEmailAsync()
        {
            string email = Request.Form["email"];

            string title = "Login";
            string token = Security.GeneratePIN();
            int expirationInMin = 2;
            string subject = "Your Temporary Budgie Login Code";

            User user = userService.Read(email);

            if (user == null)
            {
                title = "Sign Up";
                subject = "Your Budgie Signup Code";
            }


            AccessCode accessToken = new AccessCode
            {
                Email = email,
                Code = token,
                ExpiresAt = DateTimeOffset.UtcNow.AddMinutes(expirationInMin)
            };

            accessCodeService.UpdateToInvalid(email);
            accessCodeService.Create(accessToken);

            string separator = Path.DirectorySeparatorChar.ToString();
            string pathToTemplate = $"{environment.WebRootPath}{separator}templates{separator}LoginCode.html";

            using (StreamReader SourceReader = System.IO.File.OpenText(pathToTemplate))
            {
                string htmlBody = String.Format(SourceReader.ReadToEnd(), title, token, expirationInMin);

                SendEmailResponse response = await emailService.SendAsync(email, subject, htmlBody);
            }

            return Ok();
        }

        [AllowAnonymous]
        [HttpPost("authenticate")]
        public async Task<IActionResult> AuthenticateAsync()
        {
            string email = Request.Form["email"];
            string code = Request.Form["code"];

            AccessCode accessToken = accessCodeService.ReadValid(email, code);

            if (accessToken != null)
            {
                accessCodeService.UpdateToInvalid(accessToken.Email);

                User user = userService.Read(accessToken.Email);

                if (user == null)
                {
                    user = new User { Email = email.ToLower() };

                    int userId = userService.Create(user);
                    user = userService.Read(userId);
                }

                Dictionary<string, object> additionalClaims = new Dictionary<string, object>();
                additionalClaims.Add(ClaimTypes.Name, user.ID);

                string customToken = await firebaseService.GetCustomToken(user.ID.ToString(), additionalClaims);

                cache.Set<User>(CacheKey.User(user.ID.ToString()), user);

                return Ok(new { userId = user.ID, token = customToken });
            }

            return Unauthorized();
        }

        [HttpPost("invalidate_token")]
        public IActionResult InvalidateTokenAsync()
        {
            cache.Remove(CacheKey.User(HttpContext.User.Identity.Name));
            cache.Remove(CacheKey.CurrentBudget(HttpContext.User.Identity.Name));

            return Ok();
        }
    }
}
