﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Budgie.Models;
using Budgie.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Budgie.Controllers
{
    [Authorize]
    [Route("api/search")]
    public class SearchController : ControllerBase
    {
        private readonly ExpenseService expenseService;

        public SearchController(ExpenseService expenseService)
        {
            this.expenseService = expenseService;
        }

        [HttpGet("expenses")]
        public IActionResult ReadExpenses(string q = "")
        {
            int userId = int.Parse(HttpContext.User.Identity.Name);

            IEnumerable<Expense> expenses;

            if (!string.IsNullOrWhiteSpace(q))
            {
                expenses = expenseService.ReadAll(userId, q); ;
            }
            else
            {
                expenses = Enumerable.Empty<Expense>();
            }

            return Ok(new { expenses });
        }
    }
}
