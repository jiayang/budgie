﻿using System;
using System.Collections.Generic;
using Budgie.Commons;
using Budgie.Extensions;
using Budgie.Models;
using Budgie.Services;
using Budgie.Structs;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Distributed;
using Newtonsoft.Json;

namespace Budgie.Controllers
{
    [Authorize]
    [Route("api/users")]
    public class UserController : ControllerBase
    {
        private readonly UserService userService;
        private readonly ExpenseService expenseService;
        private readonly AccessCodeService accessCodeService;
        private readonly IDistributedCache cache;

        public UserController(UserService userService, ExpenseService expenseService, AccessCodeService accessTokenService, IDistributedCache cache)
        {
            this.userService = userService;
            this.expenseService = expenseService;
            this.accessCodeService = accessTokenService;
            this.cache = cache;
        }

        [AllowAnonymous]
        [HttpPost("")]
        public IActionResult Create()
        {
            string email = Request.Form["email"];
            
            User user = new User()
            {
                Email = email.ToLower()
            };

            userService.Create(user);

            return Ok();
        }

        [HttpGet("{id:int}")]
        public IActionResult Read(int id)
        {
            User user = cache.Get<User>(CacheKey.User(id.ToString()));

            if (user != null)
            {
                return Ok(new { user });
            }
            
            user = userService.Read(id);

            if (user != null)
            {
                cache.Set<User>(CacheKey.User(user.ID.ToString()), user);

                return Ok(new { user });
            }

            return NoContent();
        }

        [HttpPut("{id:int}")]
        public IActionResult Update(int id)
        {
            string budget = Request.Form["budget"];
            string budgetPeriod = Request.Form["budget_period"];
            string hasBudgetRollover = Request.Form["has_budget_rollover"];
            string budgetRolloverFrom = Request.Form["budget_rollover_from"];

            User user = new User
            {
                ID = id,
                Budget = double.Parse(budget),
                BudgetPeriod = budgetPeriod,
                HasBudgetRollover = bool.Parse(hasBudgetRollover),
                BudgetRolloverFrom = DateTimeOffset.Parse(budgetRolloverFrom)
            };

            userService.Update(user);

            cache.Remove(CacheKey.User(id.ToString()));
            cache.Remove(CacheKey.CurrentBudget(id.ToString()));

            return Ok();
        }

        [HttpPut("{id:int}/change_email")]
        public IActionResult UpdateEmail(int id)
        {
            string email = Request.Form["email"];
            string code = Request.Form["code"];
            
            AccessCode accessToken = accessCodeService.ReadValid(email, code);

            if (accessToken != null)
            {
                accessCodeService.UpdateToInvalid(accessToken.Email);
                userService.Update(id, email);

                cache.Remove(CacheKey.User(id.ToString()));

                return Ok();
            }

            return Unauthorized();
        }

        [HttpGet("{id:int}/budget_info")]
        public IActionResult ReadBudgetInfo(int id)
        {
            User user = cache.Get<User>(CacheKey.User(id.ToString()));
            double currentBudget = cache.Get<double>(CacheKey.CurrentBudget(id.ToString()));
            
            if (user == null)
            {
                user = userService.Read(id);

                if (user == null)
                {
                    return NoContent();
                }

                cache.Set<User>(CacheKey.User(user.ID.ToString()), user);
            }

            if (currentBudget <= 0)
            {
                QueryFilters queryFilters = new QueryFilters(
                    dateFrom: user.BudgetRolloverFrom,
                    dateTo: DateTimeOffset.UtcNow,
                    colours: new string[0]
                );

                IEnumerable<Expense> expenses = expenseService.ReadAll(user.ID, queryFilters);
                DateTimeOffset now = DateTimeOffset.UtcNow;

                currentBudget = user.GetCurrentBudget(expenses, now);

                DateRange dateRange = user.GetDateRangeOfBudgetPeriod(DateTimeOffset.UtcNow);

                cache.Set<double>(CacheKey.CurrentBudget(user.ID.ToString()), currentBudget, new DistributedCacheEntryOptions().SetAbsoluteExpiration(dateRange.DateTo));
            }

            return Ok(new { currentBudget, user.BudgetPeriod, user.Budget, user.HasBudgetRollover });
        }
    }
}
