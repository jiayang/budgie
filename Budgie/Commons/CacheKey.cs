﻿using System;
namespace Budgie.Commons
{
    public static class CacheKey
    {
        public static string User(string id)
        {
            return $"User:{id}";
        }

        public static string CurrentBudget(string userId)
        {
            return $"CurrentBudget:{userId}";
        }
    }
}
