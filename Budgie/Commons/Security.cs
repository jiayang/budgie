﻿using System;
using System.Security.Cryptography;
using Microsoft.AspNetCore.Cryptography.KeyDerivation;

namespace Budgie.Commons
{
    public static class Security
    {
        private static readonly Random random = new Random();

        public static string GeneratePIN()
        {
            int[] digits = new int[6];

            for (int i = 0; i < digits.Length; i++)
            {
                digits[i] = random.Next(10);
            }
            
            return String.Join("", digits);
        }

        public static byte[] GenerateSalt()
        {
            // generate a 128-bit salt using a secure PRNG
            byte[] salt = new byte[128 / 8];
            using (RandomNumberGenerator rng = RandomNumberGenerator.Create())
            {
                rng.GetBytes(salt);
            }

            return salt;
        }

        public static string HashPassword(string password, byte[] salt)
        {
            // derive a 256-bit subkey (use HMACSHA1 with 10,000 iterations)
            byte[] subkey = KeyDerivation.Pbkdf2(
                password: password,
                salt: salt,
                prf: KeyDerivationPrf.HMACSHA1,
                iterationCount: 10000,
                numBytesRequested: 256 / 8);

            string hashed = Convert.ToBase64String(subkey);

            return hashed;
        }
    }
}
