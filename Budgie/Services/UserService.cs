﻿using System;
using System.Collections.Generic;
using System.Linq;
using Budgie.Database;
using Budgie.Models;
using Budgie.Models.Settings;
using Dapper;
using Newtonsoft.Json;
using Npgsql;

namespace Budgie.Services
{
    public class UserService
    {
        private readonly PostgreSQLDatabase PgDb;

        public UserService(IPostgreSQLSettings settings)
        {
            PgDb = new PostgreSQLDatabase(settings.ConnectionString);
        }

        public int Create(User user)
        {
            using (NpgsqlConnection connection = (NpgsqlConnection)PgDb.GetConnection())
            {
                string sql = @"
                    INSERT INTO users (email)
			        VALUES (@email)
                    RETURNING id";

                var param = new { user.Email };

                int data = connection.Query<int>(sql, param)
                                     .SingleOrDefault();

                return data;
            }
        }

        public User Read(int id)
        {
            using (NpgsqlConnection connection = (NpgsqlConnection)PgDb.GetConnection())
            {
                string sql = @"
                    SELECT *
			        FROM users
			        WHERE id = @id";

                var param = new { id };

                User data = connection.Query<User>(sql, param)
                                      .SingleOrDefault();
                
                return data;
            }
        }

        public User Read(string email)
        {
            using (NpgsqlConnection connection = (NpgsqlConnection)PgDb.GetConnection())
            {
                string sql = @"
                    SELECT *
			        FROM users
			        WHERE email = @email";

                var param = new { email };

                User data = connection.Query<User>(sql, param)
                                      .SingleOrDefault();

                return data;
            }
        }

        public void Update(User user)
        {
            using (NpgsqlConnection connection = (NpgsqlConnection)PgDb.GetConnection())
            {
                string sql = @"
                    UPDATE users
        			SET budget = @budget, budget_period = @budgetPeriod, has_budget_rollover = @hasBudgetRollover, budget_rollover_from = @budgetRolloverFrom
		        	WHERE id = @id";

                var param = new { user.Budget, user.BudgetPeriod, user.HasBudgetRollover, user.BudgetRolloverFrom, user.ID };

                int count = connection.Execute(sql, param);
            }
        }

        public void Update(int id, string email)
        {
            using (NpgsqlConnection connection = (NpgsqlConnection)PgDb.GetConnection())
            {
                string sql = @"
                    UPDATE users
        			SET email = @email
		        	WHERE id = @id";

                var param = new { id, email };

                int count = connection.Execute(sql, param);
            }
        }
    }
}
