﻿using System;
using System.Linq;
using Budgie.Database;
using Budgie.Models;
using Budgie.Models.Settings;
using Dapper;
using Npgsql;

namespace Budgie.Services
{
    public class AccessCodeService
    {
        private readonly PostgreSQLDatabase PgDb;

        public AccessCodeService(IPostgreSQLSettings settings)
        {
            PgDb = new PostgreSQLDatabase(settings.ConnectionString);
        }

        public void Create(AccessCode accessCode)
        {
            using (NpgsqlConnection connection = (NpgsqlConnection)PgDb.GetConnection())
            {
                string sql = @"
                    INSERT INTO access_codes (email, code, expires_at)
                    VALUES (@email, @code, @expiresAt)";

                var param = new { accessCode.Email, accessCode.Code, accessCode.ExpiresAt };

                int count = connection.Execute(sql, param);
            }
        }

        public AccessCode ReadValid(string email, string code)
        {
            using (NpgsqlConnection connection = (NpgsqlConnection)PgDb.GetConnection())
            {
                string sql = @"
                    SELECT *
                    FROM access_codes
                    WHERE email = @email
                    AND code = @code
                    AND revoked = false
                    AND expires_at > now()";

                var param = new { email, code };

                AccessCode data = connection.Query<AccessCode>(sql, param)
                                             .SingleOrDefault();

                return data;
            }
        }

        public void UpdateToInvalid(string email)
        {
            using (NpgsqlConnection connection = (NpgsqlConnection)PgDb.GetConnection())
            {
                string sql = @"
                    UPDATE access_codes
                    SET revoked = true
                    WHERE email = @email
                    AND revoked = false";

                var param = new { email };

                int count = connection.Execute(sql, param);
            }
        }
    }
}
