﻿using System;
using Budgie.Database;
using Budgie.Models;
using Budgie.Models.Settings;
using Dapper;
using Npgsql;

namespace Budgie.Services
{
    public class LocationService
    {
        private readonly PostgreSQLDatabase PgDb;

        public LocationService(IPostgreSQLSettings settings)
        {
            PgDb = new PostgreSQLDatabase(settings.ConnectionString);
        }

        public void Create(Location location)
        {
            using (NpgsqlConnection connection = (NpgsqlConnection)PgDb.GetConnection())
            {
                string sql = @"
                    INSERT INTO locations (expense_id, name, address, latitude, longitude)
			        VALUES (@expenseId, @name, @address, @latitude, @longitude)";

                var param = new { location.ExpenseID, location.Name, location.Address, location.Latitude, location.Longitude };

                int count = connection.Execute(sql, param);
            }
        }

        public void Upsert(Location location)
        {
            using (NpgsqlConnection connection = (NpgsqlConnection)PgDb.GetConnection())
            {
                string sql = @"
                    INSERT INTO locations (expense_id, name, address, latitude, longitude)
			        VALUES (@expenseId, @name, @address, @latitude, @longitude)
			        ON CONFLICT (expense_id)
			        DO UPDATE SET name = @name, address = @address, latitude = @latitude, longitude = @longitude";

                var param = new { location.ExpenseID, location.Name, location.Address, location.Latitude, location.Longitude };

                int count = connection.Execute(sql, param);
            }
        }

        public void DeleteByExpenseID(int expenseId)
        {
            using (NpgsqlConnection connection = (NpgsqlConnection)PgDb.GetConnection())
            {
                string sql = @"
                    DELETE FROM locations
			        WHERE expense_id = @expenseId";

                var param = new { expenseId };

                int count = connection.Execute(sql, param);
            }
        }
    }
}
