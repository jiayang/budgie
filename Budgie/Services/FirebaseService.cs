﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using FirebaseAdmin;
using FirebaseAdmin.Auth;
using Google.Apis.Auth.OAuth2;
using Microsoft.AspNetCore.Hosting;

namespace Budgie.Services
{
    public class FirebaseService
    {
        public FirebaseService(IWebHostEnvironment environment)
        {
            string separator = Path.DirectorySeparatorChar.ToString();
            string pathToFirebaseCredentials = $"{environment.WebRootPath}{separator}firebase-service-account.json";
            Console.WriteLine(pathToFirebaseCredentials);
            FirebaseApp.Create(new AppOptions()
            {
                Credential = GoogleCredential.FromFile(pathToFirebaseCredentials),
            });
        }

        public async Task<string> GetCustomToken(string uid)
        {
            string customToken = await FirebaseAuth.DefaultInstance.CreateCustomTokenAsync(uid);

            return customToken;
        }

        public async Task<string> GetCustomToken(string uid, Dictionary<string, object> additionalClaims)
        {
            string customToken = await FirebaseAuth.DefaultInstance.CreateCustomTokenAsync(uid, additionalClaims);

            return customToken;
        }
    }
}
