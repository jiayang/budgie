﻿using System;
using System.Collections.Generic;
using System.Linq;
using Budgie.Controllers;
using Budgie.Database;
using Budgie.Models;
using Budgie.Models.Settings;
using Budgie.Structs;
using Dapper;
using Newtonsoft.Json;
using Npgsql;

namespace Budgie.Services
{
    public class ExpenseService
    {
        private readonly PostgreSQLDatabase PgDb;

        public ExpenseService(IPostgreSQLSettings settings)
        {
            PgDb = new PostgreSQLDatabase(settings.ConnectionString);
        }

        public int Create(Expense expense)
        {
            using (NpgsqlConnection connection = (NpgsqlConnection)PgDb.GetConnection())
            {
                string sql = @"
                    INSERT INTO expenses (user_id, name, amount, date, colour, notes, photo_url)
                    VALUES (@userId, @name, @amount, @date, @colour, @notes, @photoUrl)
                    RETURNING id";

                var param = new { expense.UserID, expense.Name, expense.Amount, expense.Date, expense.Colour, expense.Notes, expense.PhotoURL };

                int id = connection.Query<int>(sql, param)
                                   .SingleOrDefault();

                return id;
            }
        }

        public Expense Read(int id)
        {
            using (NpgsqlConnection connection = (NpgsqlConnection)PgDb.GetConnection())
            {
                string sql = @"
                    SELECT expenses.*, locations.*
			        FROM expenses
			        LEFT JOIN locations
			        ON expenses.id = expense_id
			        WHERE expenses.id = @id";

                var param = new { id };

                Expense data = connection.Query<Expense, Location, Expense>(sql, (expense, location) => { expense.Location = location; return expense; }, param)
                                         .SingleOrDefault();

                return data;
            }
        }

        public IEnumerable<Expense> ReadAll(int userId)
        {
            using (NpgsqlConnection connection = (NpgsqlConnection)PgDb.GetConnection())
            {
                string sql = @"
                    SELECT expenses.*, locations.*
			        FROM expenses
                    LEFT JOIN locations
			        ON expenses.id = expense_id
			        WHERE user_id = @userId
			        ORDER BY date DESC, created_at DESC";

                var param = new { userId };

                IEnumerable<Expense> data = connection.Query<Expense, Location, Expense>(sql, (expense, location) => { expense.Location = location; return expense; }, param);

                return data;
            }
        }

        public IEnumerable<Expense> ReadAll(int userId, string q)
        {
            q = $"%{q}%";

            using (NpgsqlConnection connection = (NpgsqlConnection)PgDb.GetConnection())
            {
                string sql = @"
                    SELECT expenses.*, locations.*
			        FROM expenses
                    LEFT JOIN locations
			        ON expenses.id = expense_id
			        WHERE user_id = @userId
                    AND expenses.name LIKE @q
			        ORDER BY date DESC, created_at DESC";

                var param = new { userId, q };

                IEnumerable<Expense> data = connection.Query<Expense, Location, Expense>(sql, (expense, location) => { expense.Location = location; return expense; }, param);

                return data;
            }
        }

        public IEnumerable<Expense> ReadAll(int userId, QueryFilters filters)
        {
            using (NpgsqlConnection connection = (NpgsqlConnection)PgDb.GetConnection())
            {
                string sql = @"
                    SELECT expenses.*, locations.*
			        FROM expenses
                    LEFT JOIN locations
			        ON expenses.id = expense_id
			        WHERE user_id = @userId
			        AND date >= @dateFrom
			        AND date <= @dateTo
			        ORDER BY date DESC, created_at DESC";

                var param = new { userId, filters.DateFrom, filters.DateTo };

                IEnumerable<Expense> data = connection.Query<Expense, Location, Expense>(sql, (expense, location) => { expense.Location = location; return expense; }, param);

                if (filters.Colours.Length > 0)
                {
                    data = data.Where(expense => filters.Colours.Contains(expense.Colour));
                }

                return data;
            }
        }

        public void Update(Expense expense)
        {
            using (NpgsqlConnection connection = (NpgsqlConnection)PgDb.GetConnection())
            {
                string sql = @"
                    UPDATE expenses
			        SET name = @name, amount = @amount, date = @date, colour = @colour, notes = @notes, photo_url = @photoUrl
			        WHERE id = @id";

                var param = new { expense.Name, expense.Amount, expense.Date, expense.Colour, expense.Notes, expense.PhotoURL, expense.ID };

                int count = connection.Execute(sql, param);
            }
        }

        public void Delete(int id)
        {
            using (NpgsqlConnection connection = (NpgsqlConnection)PgDb.GetConnection())
            {
                string sql = @"
                    DELETE FROM expenses
			        WHERE id = @id";

                var param = new { id };

                int count = connection.Execute(sql, param);
            }
        }
    }
}
