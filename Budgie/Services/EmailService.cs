﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Amazon;
using Amazon.SimpleEmail;
using Amazon.SimpleEmail.Model;
using Budgie.Models.Settings;

namespace Budgie.Services
{
    public class EmailService
    {
        private readonly IAWSSettings settings;

        public EmailService(IAWSSettings settings)
        {
            this.settings = settings;
        }

        public async Task<SendEmailResponse> SendAsync(string to, string subject, string htmlBody)
        {
            using (AmazonSimpleEmailServiceClient client = new AmazonSimpleEmailServiceClient(settings.AccessKeyID, settings.SecretAccessKey, RegionEndpoint.APSoutheast2))
            {
                Destination destination = new Destination { ToAddresses = new List<string> { to } };
                Message message = new Message
                {
                    Subject = new Content(subject),
                    Body = new Body
                    {
                        Html = new Content { Charset = "UTF-8", Data = htmlBody },
                        Text = new Content { Charset = "UTF-8", Data = htmlBody }
                    }
                };

                SendEmailRequest sendRequest = new SendEmailRequest
                {
                    Source = "jiayanglai@outlook.com", // TODO noreply@budgie.app
                    Destination = destination,
                    Message = message
                };

                return await client.SendEmailAsync(sendRequest);
            }
        }
    }
}
