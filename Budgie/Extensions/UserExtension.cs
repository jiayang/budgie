﻿using System;
using System.Collections.Generic;
using System.Linq;
using Budgie.Models;
using Budgie.Structs;
using Newtonsoft.Json;

namespace Budgie.Extensions
{
    public static class UserExtension
    {
        public static double GetCurrentBudget(this User user, IEnumerable<Expense> expenses, DateTimeOffset now)
        {
            if (user.HasBudgetRollover)
            {
                expenses = expenses.OrderBy((Expense expense) => expense.Date);

                DateRange dateRange = user.GetDateRangeOfBudgetPeriod(user.BudgetRolloverFrom);
                double budget = user.Budget;

                int index = 0;
                while (dateRange.DateTo <= now)
                {
                    Expense expense = expenses.ElementAtOrDefault(index);

                    if (expense != null && user.BudgetRolloverFrom > expense.Date)
                    {
                        index++;
                    }
                    else if (expense != null && dateRange.DateFrom <= expense.Date && dateRange.DateTo >= expense.Date)
                    {
                        budget -= expense.Amount;
                        index++;
                    }
                    else
                    {
                        if (budget < 0)
                        {
                            budget = 0;
                        }

                        dateRange = user.GetDateRangeOfBudgetPeriod(dateRange.DateTo);
                        budget += user.Budget;
                    }
                }

                return budget;
            }

            return user.Budget;
        }

        public static DateRange GetDateRangeOfBudgetPeriod(this User user, DateTimeOffset dateFrom)
        {
            switch (user.BudgetPeriod)
            {
                case "DAY":
                    {
                        DateTimeOffset dateTo = dateFrom.AddDays(1);

                        return new DateRange(dateFrom, dateTo);
                    }
                case "WEEK":
                    {
                        int diff = (7 + (dateFrom.DayOfWeek - DayOfWeek.Monday)) % 7;
                        dateFrom = dateFrom.AddDays(-1 * diff);

                        DateTimeOffset dateTo = dateFrom.AddDays(7);

                        return new DateRange(dateFrom, dateTo);
                    }
                case "MONTH":
                    {
                        DateTimeOffset dateTo = dateFrom.AddMonths(1);

                        return new DateRange(dateFrom, dateTo);
                    }
                case "YEAR":
                    {
                        DateTimeOffset dateTo = dateFrom.AddYears(1);

                        return new DateRange(dateFrom, dateTo);
                    }
                default:
                    throw new NotSupportedException($"Budget period \"{user.BudgetPeriod}\" is not supported.");
            }
        }
    }
}
