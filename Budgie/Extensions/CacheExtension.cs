﻿using System;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Caching.Distributed;
using Newtonsoft.Json;

namespace Budgie.Extensions
{
    public static class CacheExtension
    {
        public static void Set<T>(this IDistributedCache cache, string key, T value)
        {
            cache.SetString(key, JsonConvert.SerializeObject(value), new DistributedCacheEntryOptions().SetSlidingExpiration(TimeSpan.FromDays(1)));
        }

        public static void Set<T>(this IDistributedCache cache, string key, T value, DistributedCacheEntryOptions options)
        {
            cache.SetString(key, JsonConvert.SerializeObject(value), options);
        }

        public static T Get<T>(this IDistributedCache cache, string key)
        {
            string value = cache.GetString(key);

            return value == null ? default(T) : JsonConvert.DeserializeObject<T>(value);
        }
    }
}
