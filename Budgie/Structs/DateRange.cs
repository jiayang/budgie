﻿using System;
namespace Budgie.Structs
{
    public struct DateRange
    {
        public DateTimeOffset DateFrom;
        public DateTimeOffset DateTo;

        public DateRange(DateTimeOffset dateFrom, DateTimeOffset dateTo)
        {
            DateFrom = dateFrom;
            DateTo = dateTo;
        }
    }
}
