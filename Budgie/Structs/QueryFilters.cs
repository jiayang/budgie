﻿using System;
namespace Budgie.Structs
{
    public struct QueryFilters
    {
        public DateTimeOffset DateFrom;
        public DateTimeOffset DateTo;
        public string[] Colours;

        public QueryFilters(DateTimeOffset dateFrom, DateTimeOffset dateTo, string[] colours)
        {
            DateFrom = dateFrom;
            DateTo = dateTo;
            Colours = colours;
        }
    }
}
