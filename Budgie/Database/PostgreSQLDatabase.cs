﻿using System;
using System.Data;
using Npgsql;

namespace Budgie.Database
{
    public class PostgreSQLDatabase : BaseDatabase
    {
        public PostgreSQLDatabase(string connectionString) : base(connectionString)
        {
        }

        public override IDbConnection GetConnection()
        {
            NpgsqlConnection connection = new NpgsqlConnection(ConnectionString);

            connection.Open();

            Dapper.DefaultTypeMap.MatchNamesWithUnderscores = true;

            return connection;
        }
    }
}
