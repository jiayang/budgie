﻿using System;
using System.Data;

namespace Budgie.Database
{
    public abstract class BaseDatabase
    {
        public readonly string ConnectionString;

        public BaseDatabase(string connectionString)
        {
            ConnectionString = connectionString;
        }

        public abstract IDbConnection GetConnection();
    }
}
