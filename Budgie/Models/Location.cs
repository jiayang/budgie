﻿using System;
namespace Budgie.Models
{
    public class Location
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public int ExpenseID { get; set; }
    }
}
