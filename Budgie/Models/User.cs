﻿using System;
namespace Budgie.Models
{
    public class User
    {
        public int ID { get; set; }
        public string Email { get; set; }
        public double Budget { get; set; }
        public string BudgetPeriod { get; set; }
        public bool HasBudgetRollover { get; set; }
        public DateTimeOffset BudgetRolloverFrom { get; set; }
        public DateTimeOffset CreatedAt { get; set; }

        public User()
        {
        }
    }
}
