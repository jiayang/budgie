﻿using System;
namespace Budgie.Models.Settings
{
    public class PostgreSQLSettings : IPostgreSQLSettings
    {
        public string ConnectionString { get; set; }
    }

    public interface IPostgreSQLSettings
    {
        string ConnectionString { get; set; }
    }
}
