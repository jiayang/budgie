﻿using System;
namespace Budgie.Models.Settings
{
    public class AWSSettings : IAWSSettings
    {
        public string AccessKeyID { get; set; }
        public string SecretAccessKey { get; set; }
    }

    public interface IAWSSettings
    {
        string AccessKeyID { get; set; }
        string SecretAccessKey { get; set; }
    }
}
