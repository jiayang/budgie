﻿using System;
namespace Budgie.Models.Settings
{
    public class FirebaseSettings : IFirebaseSettings
    {
        public string ProjectID { get; set; }
    }

    interface IFirebaseSettings
    {
        string ProjectID { get; set; }
    }
}
