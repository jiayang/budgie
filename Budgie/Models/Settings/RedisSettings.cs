﻿using System;
namespace Budgie.Models.Settings
{
    public class RedisSettings : IRedisSettings
    {
        public string Host { get; set; }
        public string Name { get; set; }
    }

    public interface IRedisSettings
    {
        string Host { set; get; }
        string Name { set; get; }
    }
}
