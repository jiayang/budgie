﻿using System;
namespace Budgie.Models
{
    public class AccessCode
    {
        public int ID { get; set; }
        public string Email { get; set; }
        public string Code { get; set; }
        public bool Revoked { get; set; }
        public DateTimeOffset ExpiresAt { get; set; }
        public DateTimeOffset CreatedAt { get; set; }

        public AccessCode()
        {
        }
    }
}
