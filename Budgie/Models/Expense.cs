using System;

namespace Budgie.Models
{
    public class Expense
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public double Amount { get; set; }
        public DateTimeOffset Date { get; set; }
        public string Colour { get; set; }
        public string Notes { get; set; }
        public string PhotoURL { get; set; }
        public DateTimeOffset CreatedAt { get; set; }
        public int UserID { get; set; }

        public Location Location { get; set; }

        public Expense()
        {
        }
    }
}
