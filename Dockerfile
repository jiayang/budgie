﻿FROM mcr.microsoft.com/dotnet/core/sdk:3.0 AS build
WORKDIR /app

# copy csproj and restore as distinct layers
COPY *.sln .
COPY Budgie/*.csproj ./Budgie/
COPY BudgieTests/*.csproj ./BudgieTests/
RUN dotnet restore

# copy everything else and build app
COPY Budgie/. ./Budgie/
WORKDIR /app/Budgie
RUN dotnet publish -c Release -o out

FROM mcr.microsoft.com/dotnet/core/aspnet:3.0 AS runtime
WORKDIR /app
COPY --from=build /app/Budgie/out ./

EXPOSE 80

ENTRYPOINT ["dotnet", "Budgie.dll"]
